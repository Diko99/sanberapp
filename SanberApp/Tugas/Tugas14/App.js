import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Main from './components/Main.js';
import SkillScreen from './SkillScreen';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Main /> */}
        <SkillScreen />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
