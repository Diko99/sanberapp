// import {ILReact} from '../assets/index';
import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Skill = ({category, skillName, how, typeIcon, iconName}) => {
  let IconType =
    typeIcon === 'MaterialCommunityIcons' ? MaterialCommunityIcons : Icon;
  return (
    <TouchableOpacity>
      <View style={styles.container}>
        <View style={styles.imageSkill}>
          <IconType name={iconName} size={100} color="#003366" />
        </View>
        <View style={styles.meta}>
          <Text style={styles._title}>{skillName}</Text>
          <Text style={styles.teknologi}>{category}</Text>
          <Text style={styles.how}>{how}</Text>
        </View>
        <View style={styles.iconPrev}>
          <Icon
            style={styles.prev}
            color="#003366"
            name="keyboard-arrow-right"
            size={60}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Skill;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#0BCAD4',
    padding: 10,
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 10,
  },
  imageSkill: {
    width: '35%',
  },
  image: {
    justifyContent: 'center',
    width: 90,
    height: 80,
  },
  meta: {
    width: '50%',
  },
  _title: {
    fontSize: 24,
    color: '#003366',
    fontWeight: 'bold',
  },
  teknologi: {
    fontSize: 16,
    color: 'white',
  },
  iconPrev: {
    width: '15%',
    alignItems: 'center',
  },
  how: {
    fontWeight: 'bold',
    paddingRight: 10,
    textAlign: 'right',
    fontSize: 48,
    color: '#FFFFFF',
    right: 0,
  },
  prev: {
    paddingLeft: -10,
  },
});
