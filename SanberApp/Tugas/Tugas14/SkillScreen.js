import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Photo from '../Tugas13/elements/Photo/';
import {User} from '../../Tugas/Tugas13/assets/Images';

import Skill from '../Tugas14/components/Skill';
import data from './skillData.json';

const SkillsScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.profile}>
        <Photo photo={User} width={80} height={80} isSkills />
        <View style={styles.desc}>
          <Text style={styles.title}>Andiko Mahendra</Text>
          <Text style={styles.profession}>Frontend / Mobile Developer</Text>
        </View>
      </View>
      <View style={styles.header}>
        <TouchableOpacity style={styles.item_header}>
          <Text style={styles.text}>Languages</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item_header}>
          <Text style={styles.text}>Technology</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item_header}>
          <Text style={styles.text}>Framework / Library</Text>
        </TouchableOpacity>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.section}>
          <Text style={styles.main_title}>Daftar Skill</Text>
          <FlatList
            data={data.items}
            renderItem={item => {
              const result = item.item;
              return (
                <Skill
                  iconName={result.iconName}
                  typeIcon={result.iconType}
                  skillName={result.skillName}
                  category={result.categoryName}
                  how={result.percentageProgress}
                />
              );
            }}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default SkillsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingBottom: 0,
  },
  profile: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 10,
  },
  item_header: {
    borderRadius: 5,
    backgroundColor: '#0BCAD4',
    padding: 10,
  },
  text: {
    color: 'white',
  },
  desc: {
    flex: 1,
    paddingLeft: 20,
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    color: '#2B2649',
  },
  profession: {
    fontSize: 14,
    color: '#8E8E8E',
  },
  section: {
    marginTop: 20,
  },
  main_title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
});
