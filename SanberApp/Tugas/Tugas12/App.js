/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  FlatList,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoItem';
import data from './data.json';

export default function App() {
  return (
    <View style={styles.container}>
      {/* navbar */}
      <View style={styles.navBar}>
        <Image
          style={{width: 98, height: 22}}
          source={require('../Tugas12/images/logo.png')}
        />
        <View style={styles.rightNav}>
          <TouchableOpacity>
            <Icon style={styles.navItem} color="gray" name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon
              style={styles.navItem}
              color="gray"
              name="account-circle"
              size={25}
            />
          </TouchableOpacity>
        </View>
      </View>
      {/* body */}
      <View style={styles.body}>
        {/* <VideoItem video={data.items[0]} /> */}
        <FlatList
          key={item => item.id}
          data={data.items}
          renderItem={video => <VideoItem video={video.item} />}
          ItemSeparatorComponent={() => (
            <View style={{height: 0.5, backgroundColor: '#cccccc'}} />
          )}
        />
      </View>
      {/* bottom navigatoin */}
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color="red" name="home" size={25} />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color="gray" name="whatshot" size={25} />
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color="gray" name="subscriptions" size={25} />
          <Text style={styles.tabTitle}>Subscribe</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color="gray" name="folder" size={25} />
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 25,
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    paddingTop: 3,
    fontSize: 11,
    color: 'gray',
  },
});
