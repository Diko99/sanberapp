import React from 'react';
import {TextInput, StyleSheet, View} from 'react-native';

const Input = ({title, value, secureTextEntry, light, type, onChangeText}) => {
  return (
    <View style={styles.container}>
      <TextInput
        placeholder={title}
        style={styles.input}
        onChangeText={onChangeText}
        value={value}
        secureTextEntry={secureTextEntry}
        placeholderTextColor="#767676"
      />
    </View>
  );
};
export default Input;

const styles = StyleSheet.create({
  input: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 20,
    borderColor: '#767676',
  },
});
