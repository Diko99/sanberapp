import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

export default function ImageSosmed({source}) {
  return <Image source={source} style={styles.sosmed_image} />;
}

const styles = StyleSheet.create({
  sosmed_image: {
    width: 25,
    height: 25,
  },
});
