/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

const Skill = ({_title, title, persent, photo, how, level}) => {
  const MainTitle =
    _title === 1
      ? 'Bahasa Pemograman'
      : _title === 2
      ? 'Design'
      : _title === 3
      ? 'Teknologi'
      : _title === 4
      ? 'Framework / Library'
      : '';

  return (
    <View style={styles.language_programming}>
      {_title && (
        <Text
          style={{
            fontSize: 18,
            fontWeight: 'normal',
            marginBottom: 20,
            borderBottomWidth: 2,
            borderBottomColor: 'gray',
            paddingBottom: 5,
          }}>
          {MainTitle}
        </Text>
      )}
      <View style={styles.content}>
        <View style={styles.meta}>
          <Image source={photo} style={styles.image} />
          <Text style={styles.title_image}>{title}</Text>
        </View>
        <View style={styles.presentase}>
          <Text style={styles.title_content}>{level}</Text>
          <View style={{flexDirection: 'row'}}>
            <Image source={persent} style={styles.icon_persentase} />
            <Text style={styles.how}>{how}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Skill;

const styles = StyleSheet.create({
  language_programming: {
    padding: 10,
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
  },
  presentase: {
    marginLeft: 15,
  },
  title_content: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  icon_persentase: {
    width: 200,
    height: 10,
    borderRadius: 10,
    marginTop: 10,
  },
  how: {
    marginTop: 5,
    marginLeft: 10,
  },
  title_image: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  meta: {
    alignItems: 'center',
    width: 65,
  },
});
