import React from 'react';
import {StyleSheet, Text, View, ImageBackground, Image} from 'react-native';
import {
  ILBackgroundLight,
  User,
  Linkedin,
  Github,
  Youtube,
  Instagram,
  Telegram,
} from '../assets/Images';
import ImageSosmed from '../elements/Account';
import Gap from '../elements/Gap';
import Button from '../elements/Button';
import Photo from '../elements/Photo';

const Profile = ({navigation}) => {
  return (
    <ImageBackground source={ILBackgroundLight} style={styles.container}>
      <Photo photo={User} width={170} height={170} />
      <View style={styles.meta}>
        <Text style={styles.title}>Andiko Mahendra</Text>
        <Text style={styles.profession}>Frontend Developer</Text>
      </View>
      <View style={styles.social_account}>
        <ImageSosmed source={Linkedin} />
        <Gap width={20} />
        <ImageSosmed source={Github} />
        <Gap width={20} />
        <ImageSosmed source={Youtube} />
        <Gap width={20} />
        <ImageSosmed source={Instagram} />
        <Gap width={20} />
        <ImageSosmed source={Telegram} />
      </View>
      <Text style={styles.contact}>Contact Me</Text>
      <Button
        title="Details Profile"
        isDark
        onPress={() => navigation.navigate('skills')}
      />
    </ImageBackground>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    alignItems: 'center',
  },
  meta: {
    alignItems: 'center',
    marginTop: 10,
  },
  title: {
    fontSize: 19,
    color: '#2B2649',
  },
  profession: {
    fontSize: 14,
    color: '#8E8E8E',
  },
  social_account: {
    flexDirection: 'row',
    marginTop: 50,
    borderBottomWidth: 2,
    borderBottomColor: 'gray',
    paddingBottom: 10,
    marginBottom: 20,
  },
  contact: {
    fontSize: 16,
    marginBottom: 40,
    borderBottomWidth: 2,
    borderBottomColor: 'gray',
  },
});
