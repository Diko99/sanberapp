import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import Photo from '../elements/Photo';
import {
  User,
  Persentase,
  Skill1,
  Skill2,
  Skill3,
  Skill4,
  Skill5,
  Skill6,
  Skill7,
  Skill8,
  Skill9,
} from '../assets/Images';
import Skill from '../elements/Skill';

const Skills = () => {
  return (
    <View style={styles.container}>
      <View style={styles.profile}>
        <Photo photo={User} width={80} height={80} isSkills />
        <View style={styles.desc}>
          <Text style={styles.title}>Andiko Mahendra</Text>
          <Text style={styles.profession}>Frontend / Mobile Developer</Text>
        </View>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.section}>
          <Text style={styles.main_title}>Daftar Skill</Text>
          <Skill
            level="Basic"
            photo={Skill1}
            title="Javascript"
            persent={Persentase}
            how="80%"
            _title={1}
          />
          <Skill
            level="Basic"
            photo={Skill2}
            title="Figma"
            persent={Persentase}
            how="80%"
            _title={2}
          />
          <Skill
            level="Basic"
            photo={Skill3}
            title="Inkscape"
            persent={Persentase}
            how="80%"
          />
          <Skill
            level="Basic"
            photo={Skill4}
            title="Github"
            persent={Persentase}
            how="80%"
            _title={3}
          />
          <Skill
            level="Basic"
            photo={Skill5}
            title="Gitlab"
            persent={Persentase}
            how="80%"
          />
          <Skill
            level="Basic"
            photo={Skill6}
            title="ReactJs"
            persent={Persentase}
            how="80%"
            _title={4}
          />
          <Skill
            level="Basic"
            photo={Skill6}
            title="React-Native"
            persent={Persentase}
            how="80%"
          />
          <Skill
            level="Basic"
            photo={Skill7}
            title="Boostrap"
            persent={Persentase}
            how="80%"
          />
          <Skill
            level="Basic"
            photo={Skill8}
            title="Sass/Scss"
            persent={Persentase}
            how="80%"
          />
          <Skill
            level="Basic"
            photo={Skill9}
            title="Material UI"
            persent={Persentase}
            how="80%"
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Skills;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  profile: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  desc: {
    flex: 1,
    paddingLeft: 20,
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    color: '#2B2649',
  },
  profession: {
    fontSize: 14,
    color: '#8E8E8E',
  },
  section: {
    marginTop: 20,
  },
  main_title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
});
