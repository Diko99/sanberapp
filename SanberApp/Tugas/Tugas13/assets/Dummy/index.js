import {
  Skill1,
  Skill2,
  Skill3,
  Skill4,
  Skill5,
  Skill6,
  Skill7,
  Skill8,
  Skill9,
} from '../Images/index';

const data = [
  {
    id: 1,
    imageUri: Skill1,
    title: 'Javascript',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 2,
    imageUri: Skill2,
    title: 'Figma',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 3,
    imageUri: Skill3,
    title: 'Inkscape',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 4,
    imageUri: Skill4,
    title: 'Github',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 5,
    imageUri: Skill5,
    title: 'Gitlab',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 6,
    imageUri: Skill6,
    title: 'ReactJs',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 7,
    imageUri: Skill6,
    title: 'React Native',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 8,
    imageUri: Skill7,
    title: 'Boostrap',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 9,
    imageUri: Skill8,
    title: 'SASS/SCSS',
    level: 'Basic',
    how: '80%',
  },
  {
    id: 9,
    imageUri: Skill9,
    title: 'Material UI',
    level: 'Basic',
    how: '80%',
  },
];

export default data;
