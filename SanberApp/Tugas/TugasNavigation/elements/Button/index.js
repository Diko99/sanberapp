import React from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';

const Button = ({title, type, onPress, disable, isDark}) => {
  return (
    <TouchableOpacity style={styles.container(isDark)} onPress={onPress}>
      <Text style={styles.button(isDark)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: isDark => ({
    borderRadius: 5,
    marginTop: 20,
    marginBottom: 20,
  }),
  button: isDark => ({
    textAlign: 'center',
    padding: 10,
    width: isDark && 250,
    color: isDark ? 'white' : '#1E1939',
    fontWeight: isDark ? 'normal' : 'bold',
    backgroundColor: isDark ? '#1E1939' : 'white',
    borderRadius: 5,
    fontSize: 18,
  }),
});
