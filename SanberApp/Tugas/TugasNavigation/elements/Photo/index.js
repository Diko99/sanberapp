import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const Photo = ({photo, width, height, isSkills}) => {
  return (
    <View style={styles.profile(width, height, isSkills)}>
      <Image source={photo} style={styles.photo(width, height)} />
    </View>
  );
};

export default Photo;

const styles = StyleSheet.create({
  profile: (width, height, isSkills) => ({
    marginTop: isSkills ? 0 : 50,
    width: width,
    height: height,
    borderRadius: width / 2,
    backgroundColor: '#474747',
    alignItems: 'center',
    justifyContent: 'center',
  }),
  photo: (width, height) => ({
    width: width - 10,
    height: height - 10,
    borderRadius: width / 2,
  }),
});
