import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

const Link = ({title, type, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text}> {title} </Text>
    </TouchableOpacity>
  );
};

export default Link;

const styles = StyleSheet.create({
  text: {
    color: 'white',
    textAlign: 'center',
    marginBottom: 30,
    textTransform: 'capitalize',
  },
});
