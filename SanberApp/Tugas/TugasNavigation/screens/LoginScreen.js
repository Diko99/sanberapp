/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
  View,
  Image,
} from 'react-native';
import {ILBackgroundDark} from '../assets/Images';
import Input from '../elements/Input';
import Gap from '../elements/Gap';
import Button from '../elements/Button';
import Link from '../elements/Link';
import {Logo} from '../assets/Icons';

const Login = ({navigation}) => {
  return (
    <ImageBackground source={ILBackgroundDark} style={styles.container}>
      <Image source={Logo} style={{width: 100, height: 100}} />
      <Text style={styles.desc}>Pleast login for find a something</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.form}>
          <Input title="Username" />
          <Gap height={20} />
          <Input title="Password" />
          <Gap height={20} />
          <Input title="Confirm Password" />
          <Gap height={20} />
          <Button title="Sign In" onPress={() => navigation.navigate('MyDrawer') } />
        </View>
      </ScrollView>
      <Link title={"Don't Have Account ? Register "} />
    </ImageBackground>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    paddingTop: 60,
  },
  desc: {
    fontSize: 14,
    marginTop: 30,
    color: '#FBFBFB',
  },
  form: {
    marginTop: 25,
  },
});
