import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, HeaderTitle} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Login from './screens/LoginScreen';
import Profile from './screens/Profile';
import SkillsScreen from '../Tugas14/SkillScreen';
import ProjectScreen from './screens/ProjectScreen';
import AddScreen from './screens/AddScreen';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="SkillScreen" component={SkillsScreen} />
    <Tabs.Screen name="ProjectScreen" component={ProjectScreen} />
    <Tabs.Screen name="AddScreen" component={AddScreen} />
  </Tabs.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Skills">
    <Drawer.Screen name="Profile" component={Profile} />
    <Drawer.Screen name="Skills" component={TabsScreen} />
  </Drawer.Navigator>
);

const StackScreen = () => (
  <Stack.Navigator
    initialRouteName="LoginScreen"
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="LoginScreen" component={Login} />
    <Stack.Screen name="MyDrawer" component={DrawerScreen} />
  </Stack.Navigator>
);

export default function App() {
  return (
    <NavigationContainer>
      <StackScreen />
    </NavigationContainer>
  );
}
