import ILBackgroundDark from './background-dark.png';
import ILBackgroundLight from './background-light.png';
import User from './dummyUser.jpeg';
import Linkedin from './linkedin.png';
import Github from './github.png';
import Youtube from './youtube.png';
import Instagram from './instagram.png';
import Telegram from './telegram.png';
import Persentase from './persentase.png';
import Skill1 from './skill-1.png';
import Skill2 from './skill-2.png';
import Skill3 from './skill-3.png';
import Skill4 from './skill-4.png';
import Skill5 from './skill-5.png';
import Skill6 from './skill-6.png';
import Skill7 from './skill-7.png';
import Skill8 from './skill-8.png';
import Skill9 from './skill-9.png';

export {
  ILBackgroundDark,
  ILBackgroundLight,
  User,
  Linkedin,
  Youtube,
  Github,
  Instagram,
  Telegram,
  Persentase,
  Skill1,
  Skill2,
  Skill3,
  Skill4,
  Skill5,
  Skill6,
  Skill7,
  Skill8,
  Skill9,
};
