import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button,
} from 'react-native';

import data from './data.json';
import {ScrollView} from 'react-native-gesture-handler';

const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      totalPrice: 0,
    };
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }

  updatePrice(price) {
    //? #Soal Bonus (10 poin)
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    // Kode di sini
  }
  render() {
    const username = this.props.route.params;
    return (
      <View style={styles.container}>
        <View
          style={{
            minHeight: 50,
            width: DEVICE.width * 0.88 + 20,
            marginVertical: 8,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text>
              Hai,{'\n'}
              <Text style={styles.headerText}>{username}</Text>
            </Text>
            <Text style={{textAlign: 'right'}}>
              Total Harga{'\n'}
              <Text style={styles.headerText}>tempat total harga</Text>
            </Text>
          </View>
          <View />
          <TextInput
            style={{backgroundColor: 'white', marginTop: 8}}
            placeholder="Cari barang.."
            onChangeText={searchText => this.setState({searchText})}
          />
        </View>
        <FlatList
          data={data.produk}
          renderItem={({item}) => <ListItem data={item} />}
        />
      </View>
    );
  }
}

class ListItem extends React.Component {
  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }
  render() {
    const res = this.props.data;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.itemContainer}>
          <Image
            source={{uri: res.gambaruri}}
            style={styles.itemImage}
            resizeMode="contain"
          />
          <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
            {res.nama}
          </Text>
          <Text style={styles.itemPrice}>
            {this.currencyFormat(Number(res.harga))}
          </Text>
          <Text style={styles.itemStock}>Sisa stok: {res.stock}</Text>
          <TouchableOpacity
            onPress={event => {
              this.buyNow, res.itemPrice;
            }}>
            <View style={styles.itemButton}>
              <Text style={styles.buttonText}>BELI</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    marginBottom: 30,
  },
  itemImage: {
    borderRadius: 10,
    height: 150,
  },
  itemName: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 5,
  },
  itemPrice: {
    marginTop: 5,
    fontWeight: 'normal',
    fontSize: 16,
  },
  itemStock: {
    fontWeight: 'bold',
    marginTop: 5,
    fontSize: 16,
  },
  itemButton: {
    marginTop: 15,
    backgroundColor: 'blue',
    paddingVertical: 12,
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
});
